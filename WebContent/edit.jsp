<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		<h1><c:out value="${loginUser.name}"/></h1>
		<h2><c:out value="${loginUser.account}"/></h2>
		<h3><c:out value="${loginUser.description}"/></h3>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

	<form action="edit" method="post">
		<textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value = "${preMessage.text}" />
			<c:if test="${ not empty errorMessages }"><c:out value = "${proMessage.text}" /></c:if>
		</textarea>
		<input name="preMessageId" value="${preMessage.id}" id="preMessageId" type="hidden" />
		<br /> <input type="submit" value="更新">（140文字まで）
	</form>


</body>
</html>