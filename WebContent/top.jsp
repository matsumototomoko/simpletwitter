<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description}" />
				</div>
			</div>
		</c:if>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="form-area">
			<c:if test="${ isShowMessageForm }">
				<form action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box">
					</textarea>
					<br /> <input type="submit" value="つぶやく">（140文字まで）
				</form>
			</c:if>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<!-- 日付の絞り込み -->
		<form action="./"  method="get">
			日付<input name="start" value="${start}" id="start" type="date" />
			～<input name="end" value="${end}" id="end" type="date" />
			<input type="submit" value="絞り込み">
		</form>
		<!-- リストに格納されたmessages -->
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<!-- 選択されたユーザーのユーザーアイディーを出し、それに関連するメッセージを出す。-->
						<span class="account">
						 <a href="./?user_id=<c:out value="${message.userId}"/>">
							 <c:out value="${message.account}" />
						 </a>
						</span>
								<span class="name"> <c:out value="${message.name}" /></span>
					</div>
					<pre class="text"><c:out value="${message.text}" /></pre>

					<!-- 返信の表示 -->
					<c:if  test="${ not empty userComments}">
					<c:forEach items="${userComments}" var="userComment">
					<c:if test="${userComment.messageId == message.id}">
						<pre><c:out value="${userComment.text}" /></pre>
							<p><c:out value="${userComment.name}" />
								<c:out value="${userComment.account}" />
								<br><c:out value="${userComment.createdDate}" />
							</p>
						</c:if>
						</c:forEach>
					</c:if>
					<!--top.jspのeditクラスからEditServletにidとつぶやきの編集を渡す渡されるパラメータid=?-->
					<div class="edit">
						<c:if test="${loginUser.id == message.userId }">
							<form action="edit"  method="get"><br />
								<!-- message.idは -->
								<input name="id" value="${message.id}" id="id" type="hidden" />
								<input type="submit" value="編集">
							</form>
						</c:if>
					</div>

					<!--返信する-->
					<div class="response">
					<c:if test="${ isShowMessageForm }">
							<form action="comment"  method="post"><br />
								<!-- message.idは -->
								<input name = messageId value = "${message.id}" id = messageId type = "hidden" >
								<textarea name="text" cols="100" rows="5" class="tweet-box">
								</textarea>
								<input type="submit" value="返信">
							</form>
					</c:if>
					</div>
					<!-- daleteクラスからDeleteMessageServletにidを渡す-->
					<div class="delete">
						<c:if test="${loginUser.id == message.userId}">
							<form action="deleteMessage"  method="post"><br />
								<input name="id" value="${message.id}" id="id" type="hidden" />
								<input name="userId" value="${message.userId}" id="userId" type="hidden" />
								<input name="loginUserId" value="${loginUser.id}" id="loginUserId" type="hidden"/>
								<input type="submit" value="削除">
						</form>
						</c:if>
					</div>

					<div class="comment">

					</div>

					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
				</div>
			</c:forEach>
		</div>
		<div class="copyright">Copyright(c)MatsumotoTomoko</div>
	</div>
</body>
</html>