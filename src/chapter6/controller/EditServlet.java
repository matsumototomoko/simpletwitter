package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//過去ツイートに対応するidをtop.jsp送信ボタンから取得
		String preMessageId = request.getParameter("id");

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		Message preMessages = null;
		if (!StringUtils.isBlank(preMessageId) && preMessageId.matches("^[0-9]+$")) {
			preMessages = new MessageService().select(Integer.parseInt(preMessageId));
		}
		//編集ボタンに対応するidのメッセージが欲しい
		if (preMessages == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		//プレメッセージを格納
		request.setAttribute("preMessage", preMessages);
		request.getRequestDispatcher("edit.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		Message message = new Message();
		Message preMessages = new Message();
		String text = request.getParameter("text");
		String preMessageId = request.getParameter("preMessageId");

		if (isValid(text, errorMessages)) {
			message.setText(text);
			message.setId(Integer.parseInt(preMessageId));
			new MessageService().update(message);
			response.sendRedirect("./");
		} else {
			request.setAttribute("errorMessages", errorMessages);
			preMessages.setText(text);
			preMessages.setId(Integer.parseInt(preMessageId));
			request.setAttribute("preMessage", preMessages);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}
	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
