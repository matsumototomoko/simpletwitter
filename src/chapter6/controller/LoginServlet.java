package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	//まずはログイン画面を表示
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
    	//	login.jspに飛びます。Dispatcherはパスを指名。forwardで転送
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    //ログインjspでもらった情報をUserServiceに渡す。
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String accountOrEmail = request.getParameter("accountOrEmail");
        String password = request.getParameter("password");

        //UserServiceからsellectメソッドを呼び出し
        User user = new UserService().select(accountOrEmail, password);
        if (user == null) {
            List<String> errorMessages = new ArrayList<String>();
            errorMessages.add("ログインに失敗しました");
            request.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        HttpSession session = request.getSession();
        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }
}
