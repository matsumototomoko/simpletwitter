package chapter6.beans;

import java.util.Date;

public class UserComment {


	private int messageId;
	private String account;
	private String name;
	private String text;
	private Date createdDate;

	// getter/setterは省略されているので、自分で記述しましょう。
		public String getAccount() {
			return account;
		}
		public void setAccount(String account) {
			this.account = account;
		}

		public int getMessageId() {
			return messageId;
		}

		public void setMassageId(int messageId) {
			this.messageId = messageId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public Date getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(Date createdDate) {
	 	   this.createdDate = createdDate;
		}
}
