package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.UserComment;
import chapter6.dao.CommentDao;
import chapter6.dao.UserCommentDao;

public class CommentService {

	public void insert(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().insert(connection, comment);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserComment> select(String messageId) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			/*
			   * idをnullで初期化
			   * ServletからuserIdの値が渡ってきていたら
			   * 整数型に型変換し、idに代入
			   */
			Integer id = null;
			if (!StringUtils.isEmpty(messageId)) {
				id = Integer.parseInt(messageId);
			}

			/*
			    * messageDao.selectに引数としてInteger型のidを追加
			    * idがnullだったら全件取得する
			    * idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する
			    */
			List<UserComment> userComments = new UserCommentDao().select(connection, id, LIMIT_NUM);
			commit(connection);

			return userComments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
