package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                  // user_id
            sql.append("    ?, ");                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");   // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void delete(Connection connection, String id) {

        PreparedStatement ps = null;

        	try {
                StringBuilder sql = new StringBuilder();
                sql.append("DELETE FROM messages  ");
                sql.append("    WHERE  ");
                sql.append("    id = ?  ");
              // deleted_date
                sql.append(";");

                ps = connection.prepareStatement(sql.toString());

                ps.setString(1, id);

                ps.executeUpdate();

            } catch (SQLException e) {
                throw new SQLRuntimeException(e);
            } finally {
                close(ps);
            }
    }


    //変更テキストとidを持ってくる
    public Message select(Connection connection, int preMessageId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("	SELECT   ");
            sql.append("	*	");
            sql.append("    FROM	 ");
            sql.append("    messages ");
            sql.append("    where ");
            sql.append("    id = ? ");
            sql.append(";");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, preMessageId);

            ResultSet rs = ps.executeQuery();

            List<Message> preMessages = getPreMessage(rs);
            if(preMessages.isEmpty()) {
            	return null;
            }else {
            	return preMessages.get(0);
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    private List<Message> getPreMessage(ResultSet rs) throws SQLException {

		List<Message> preMessages = new ArrayList<Message>();
		try {
			while (rs.next()) {
				Message message = new Message();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setCreatedDate(rs.getTimestamp("created_date"));
				message. setUpdatedDate(rs.getTimestamp("updated_date"));

				preMessages.add(message);
			}
			return preMessages;
		} finally {
			close(rs);
		}
	}

  //変更したものアップデート editはここ
    public void update(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("	UPDATE messages  ");
            sql.append("	SET	");
            sql.append("    text = ?,	 ");
            sql.append("    updated_date = CURRENT_TIMESTAMP     ");
            sql.append("    WHERE ");
            sql.append("    id = ? ");
            sql.append(";");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getText());
            ps.setInt(2, message.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
