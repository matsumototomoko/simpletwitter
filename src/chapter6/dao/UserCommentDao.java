package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection, Integer id, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    comments.text as text, ");
			sql.append("    comments.user_id as user_id, ");
			sql.append("    comments.message_id as message_id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserComment> userComments = toUserComments(rs);
			return userComments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserComments(ResultSet rs) throws SQLException {

		List<UserComment> userComments = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				UserComment userComment = new UserComment();
				userComment.setText(rs.getString("text"));
				userComment.setName(rs.getString("name"));
				userComment.setAccount(rs.getString("account"));
				userComment.setMassageId(rs.getInt("message_id"));
				userComment.setCreatedDate(rs.getTimestamp("created_date"));

				userComments.add(userComment);
			}
			return userComments;
		} finally {
			close(rs);
		}
	}

}
